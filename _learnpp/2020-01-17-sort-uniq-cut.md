---
layout: learnpp
title: 'Unix Command: sort'
status: publish
published: true
author:
  display_name: dcodernz
  login: dcodernz
  email: no-reply@thh.com
  url: http://www.thheuer.com
author_login: dcodernz
author_email: no-reply@thh.com
author_url: http://www.thheuer.com
categories:
- Computer
- Science
- Unix
tags: [Computer, Science, Unix Commands]
comments: []
---
Unix commands are at the heart of many OS.
```sort | uniq | cut | rev```
are great examples of how you can do many things within (ba|z)sh and other shells.

### sort

`sort` is a very useful atomic tool in the true spirit of Unix (Linux, BSD).
You can use it, as the name implies, to sort the input lines. This is useful if you have a big file
with many lines and you need to do any kind of sort. You can sort by string or by natural numbers:

`ls -1 | sort`

for example, sorts file names in the current directory.

```for (( i=100;i>0;--i )); do echo $i; done | sort -n```

sorts the input numerically rather than alphabetically / naturally.

In conjunction with uniq (sort then uniq), you can show the unique items in sorted order.

Note that uniq only "uniquifies" lines that follow each other that are the same. This is simply
because otherwise the command could not operate on the input stream.

e.g.

```cat input | sort | uniq```

outputs the file input, streams it to the sort command (which has to get the whole input), and then pipes the output stream
to uniq, only printing the next line if it was not a repetition of the previous line, therefore getting the globally unique lines.
