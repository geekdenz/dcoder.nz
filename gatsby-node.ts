import {GatsbyNode} from "gatsby";

const getDateFromFilename = (filename: string): string | undefined => {
  const regex = /\d{4}-\d{2}-\d{2}/;
  const found = filename.match(regex);
  return found ? found[0] : undefined;
};

export const onCreateNode: GatsbyNode['onCreateNode'] = ({ node, actions, getNode }) => {
  const { createNodeField } = actions;

  if (node.internal.type === "MarkdownRemark") {
    const parent = getNode(node.parent as string); // TODO: check as string here appropriate
    if (parent && parent.internal.type === "File") {
      const filename = parent.name as string;
      const title = filename?.substring('YYYY-MM-DD-'.length).split('-').map(word => word.charAt(0).toUpperCase() + word.substring(1)).join(' ');
      const date = getDateFromFilename(filename);

      if (date) {
        createNodeField({
          node,
          name: "filename",
          value: filename,
        });
        createNodeField({
          node,
          name: "title",
          value: title,
        });
        createNodeField({
          node,
          name: "fileDate",
          value: date,
        });
      }
    }
  }
};

export const createSchemaCustomization: GatsbyNode['createSchemaCustomization'] = ({ actions }) => {
  const { createTypes } = actions;
  createTypes(`
    type MarkdownRemark implements Node {
      fields: Fields
    }
    type Fields {
      title: String
      fileDate: Date @dateformat
    }
  `);
};

