<?php
die('not yet!');
require_once 'class_rss_generator.inc.php';

$rss = new rss_generator('dcoder.co.nz');

$rss->encoding = 'UTF-8';
$rss->language = 'en_GB';
$rss->description = 'dcoder\'s rss';
$rss->generator = 'dcoder';
$rss->link = 'http://dcoder.co.nz';


function getOne($mysqli, $sql) {
  if ($result = $mysqli->query($query)) {
    /* fetch associative array */
    while ($row = $result->fetch_assoc()) {
      //printf ("%s (%s)\n", $row["Name"], $row["CountryCode"]);
      $url = $row['url'];
      return $url;
    }
    /* free result set */
    $result->free();
  }
  return null;
}

function getUrl($mysqli, $shortcut) {
  $query = "SELECT url FROM rss WHERE shortcut='".
      $mysqli->mysqli_real_escape_string($s) ."'";
  $url = getOne($mysqli, $query);
  return $url;
}

$mysqli = new mysqli("localhost", "rss", "roaBlesh0", "rss");
/* check connection */
if ($mysqli->connect_errno) {
  printf("Connect failed: %s\n", $mysqli->connect_error);
  exit();
}


if (isset($_GET['s'])) {
  $s = $_GET['s'];
  $url = getUrl($mysqli, $s);
} else {
  $url = $_GET['url'];
}

$content = file_get_contents($url);
foreach ($http_response_header as $header) {
  if (strpos($header, 'Date:') === 0) {
    $date = substr($header, strlen('Date: '));
    break;
  }
}
if (isset($s)) {
  $cachedContent = getCached($mysqli, $s);
  if ($cachedContent === $content) {
    oldFeed($mysqli);
  } else {

  }
}

$start = stripos($content, '<title>') + strlen('<title>');
$len = stripos($content, '</title>') - $start;
$title = substr($content, $start, $len);

$items = array(
  array(
    'title' => $title,
    'description' => $content,
    'pubDate' => $date,
    'link' => $_GET['url'],
  ),
);

$html = $rss->get($items);

header('Content-Type: application/rss+xml; charset=utf-8');
echo $html;

/* close connection */
mysqli->close();
