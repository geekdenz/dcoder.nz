define({
  "name": "xsiteauth",
  "version": "0.1.0",
  "description": "XSiteAuth authenticates users across domains.",
  "title": "XSiteAuth - API Documentation",
  "url": "https://example.com/xsiteauth/v1",
  "sampleUrl": false,
  "apidoc": "0.2.0",
  "generator": {
    "name": "apidoc",
    "time": "2016-08-28T02:38:14.473Z",
    "url": "http://apidocjs.com",
    "version": "0.16.1"
  }
});
