define({ "api": [
  {
    "type": "post",
    "url": "/get",
    "title": "Request User information and Auth Token",
    "name": "GetUserAuth",
    "group": "Server_API",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sharedSecret",
            "description": "<p>Sites' shared secret API key.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user",
            "description": "<p>User's unique username.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "group",
            "description": "<p>A user group.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "user Example",
          "content": "{\n  \"sharedSecret\": \"0123456789ABCDEF0123456789ABCDEF\",\n  \"user\": \"jesse\"\n}",
          "type": "json"
        },
        {
          "title": "group Example",
          "content": "{\n  \"sharedSecret\": \"0123456789ABCDEF0123456789ABCDEF\",\n  \"group\": \"all-media\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "authToken",
            "description": "<p>Randomly generated auth token to use in request.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "url",
            "description": "<p>URL to call with authToken as parameter.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"authToken\": \"0123456789ABCDEF0123456789ABCDEF\",\n  \"url\": \"https://example.com/xsiteauth/v1/auth\"\n}",
          "type": "json"
        }
      ]
    },
    "examples": [
      {
        "title": "Request token",
        "content": "curl -d '{\n    \"sharedSecret\": \"0123456789ABCDEF0123456789ABCDEF\",\n    \"user\": \"jesse\"\n}' -i https://example.com/xsiteauth/v1/get\ncurl -d '{\n    \"sharedSecret\": \"0123456789ABCDEF0123456789ABCDEF\",\n    \"group\": \"all-media\"\n}' -i https://example.com/xsiteauth/v1/get",
        "type": "bash"
      },
      {
        "title": "AJAX Request to login",
        "content": "fetch('https://example.com/xsiteauth/v1/auth?token=0123456789ABCDEF0123456789ABCDEF')\n.then((response) => {return response.json()})\n.then((json) => {\n    // do something with json data\n});",
        "type": "js"
      },
      {
        "title": "Link to login",
        "content": "<a href=\"https://example.com/xsiteauth/v1/auth?token=0123456789ABCDEF0123456789ABCDEF&redirect=some_path\">\n    Login and go to some page\n</a>",
        "type": "html"
      }
    ],
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFound",
            "description": "<p>The id of the User was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"UserNotFound\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "xsiteauth/api.php",
    "groupTitle": "Server_API"
  }
] });
