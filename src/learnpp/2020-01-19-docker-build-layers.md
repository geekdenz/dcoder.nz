---
layout: learnpp
title: 'Docker: Layers in Build'
status: publish
published: true
author:
  display_name: dcodernz
  login: dcodernz
  email: no-reply@thh.com
  url: http://www.thheuer.com
author_login: dcodernz
author_email: no-reply@thh.com
author_url: http://www.thheuer.com
categories:
- Docker
tags: [Docker, Layers, Build]
comments: []
date: "2020-01-19"
title2: "Docker build layers"
---

Docker build layers are an amazing concept and can greatly speedup
your development workflow.

```Docker
docker build -t mydockerimage:latest .
```

### Docker Layers

A layer in Docker is a snapshot of the most atomic part of a Docker build.
This means, that Docker will not need to rerun the part of the build that
is part of a layer.

Layers are created by commands that have an internally consistent state.

For example a RUN command does not get called again unless it depends on
another non-deterministic directive in the Dockerfile.

Non-deterministic directives are for example:

```Docker
COPY https://mylibrarydownload.org/my-lib.tar.gz
```

You can easily make this deterministic by downloading the file first and
then using

```Docker
ADD my-lib.tar.gz
```

This way, Docker can create a layer from it and cache the result.

Another example where this is helpful is ```apt-get```:

You could write:

```Docker
RUN apt-get install curl
```

and then later discover, you also need ```less```.

Instead of adding less to the line like so:

```Docker
RUN apt-get install curl less # don't do this!
```

you can add another apt-get RUN command like so:

```Docker
RUN apt-get install curl
RUN apt-get install less
```

This way, Docker can use the cached layer from

```Docker
RUN apt-get install curl
```

and just add

```Docker
RUN apt-get install less
```

Also, if you upload to the Docker Hub or even your own container registry,
you will be saving space for other containers that install the same layer.
