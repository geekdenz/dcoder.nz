---
layout: learnpp
title: Node Version Manager - .nvmrc
status: publish
published: true
author:
  display_name: dcodernz
  login: dcodernz
  email: no-reply@thh.com
  url: http://www.thheuer.com
author_login: dcodernz
author_email: no-reply@thh.com
author_url: http://www.thheuer.com
categories:
- Computer
- Science
- Ideas
- Factory
tags: [Computer, Science, Ideas, Factory]
comments: []
date: "2020-01-15"
title2: "Nvmrc in project directory"
---

Want to manage NVM correctly on a project basis?

### .nvmrc in Project Directory

You can put your .nvmrc file into your project directory and ```nvm use``` will load the
correct node version for you. If your shell is setup correctly, it should automatically
load the file if you ```cd $project_dir``` into it:

```node -v > .nvmrc```

creates the file for you.
