import React from 'react'
import Layout from '../components/Layout'
import Typography from "@mui/material/Typography";
const MapPage = () => {
    return (
        <Layout>
            <Typography variant="h2">Map</Typography>
            <iframe width="425" height="350"
                    src="https://www.openstreetmap.org/export/embed.html?bbox=175.53362846374515%2C-40.38578183826233%2C175.67696571350098%2C-40.326524090927805&amp;layer=mapnik"
                    style={{
                        border: '1px solid black'
                    }}></iframe>
            <br/><small><a target="_blank" href="https://www.openstreetmap.org/#map=14/-40.3562/175.6053">View Larger Map</a></small>
        </Layout>
    )
}

export default MapPage
