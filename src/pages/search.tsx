import * as React from 'react'
import {useEffect} from 'react'
import Layout from '../components/Layout'

import {graphql, useStaticQuery} from "gatsby"
import { useGatsbyPluginFusejs } from 'react-use-fusejs'

import Typography from "@mui/material/Typography";

const BlogPage = () => {
    const data = useStaticQuery(graphql`
    {
      fusejs {
        publicUrl
      }
    }
  `)

    const [query, setQuery] = React.useState('')
    const [fusejs, setFusejs] = React.useState(null)
    const result = useGatsbyPluginFusejs(query, fusejs)

    const fetching = React.useRef(false)

    useEffect(() => {
        if (!fetching.current && !fusejs && query) {
            fetching.current = true

            fetch(data.fusejs.publicUrl)
                .then((res) => res.json())
                .then((json) => setFusejs(json))
        }
        console.log(result)
    }, [fusejs, query])

    return (
        <Layout>
            <Typography variant="h2">Search</Typography>
            <input
                type="text"
                value={query}
                onChange={(e) => setQuery(e.target.value)}
                placeholder="Search..."
            />
        </Layout>
    )
}

export default BlogPage;
