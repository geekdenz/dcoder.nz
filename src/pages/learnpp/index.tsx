import * as React from 'react';
import Layout from '../../components/Layout'
import {graphql, Link} from "gatsby";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
const LearnPage = ({data}: {data: unknown}) => {
    return (
        <Layout>
            <Typography variant="h2">Learn++</Typography>
            {data.allMarkdownRemark.nodes.map(node => {

                return (
                    <article key={node.parent.id}>
                        {/*<h2>{node.frontmatter.title}</h2>*/}
                        <Link to={encodeURIComponent(node.parent.name)} sx={{
                            display: 'block',
                            color: 'text.primary',
                            textDecoration: 'none',
                            textDecorationLine: 'none',
                            textDecorationColor: 'none',
                            textDecorationSkipInk: 'none',
                            fontSize: '1.5rem',
                            fontStyle: 'normal'
                        }}>
                            <Button>
                                {node.fields.title}
                            </Button>
                            <Typography sx={{
                                display: 'block',
                                color: 'text.secondary',
                                textDecoration: 'none',
                                fontStyle: 'italic'
                            }}>
                                Posted: {node.fields.fileDate}
                            </Typography>
                            <Typography sx={{
                                display: 'block',
                                color: 'text.primary',
                                textDecoration: 'none',
                                textDecorationLine: 'none',
                                fontSize: '1rem',
                                fontStyle: 'normal'
                            }}>
                                {node.excerpt}
                            </Typography>
                        </Link>
                    </article>
                );
            } )}
        </Layout>
    )
}

export const query = graphql`
query {
    allMarkdownRemark(
        sort: {fields: {fileDate: DESC}}
        filter: {frontmatter: {layout: {eq: "learnpp"}}}
    ) {
        nodes {
          parent {
            ... on File {
              id
              name
            }
          }
          fields {
            fileDate(formatString: "MMMM, DD YYYY")
            title
          }
          excerpt
        }
    }
}
`
export default LearnPage;
