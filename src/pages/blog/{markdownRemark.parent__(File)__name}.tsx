import * as React from 'react'
import { graphql } from 'gatsby'
import Layout from '../../components/Layout'

const BlogPost = ({data}: {data: unknown}) => {
    return (
            <div>
                <Layout>
                    {
                        !data.markdownRemark.html.includes(data.markdownRemark.frontmatter.title) ? (
                            <h1>{data.markdownRemark.frontmatter.title}</h1>
                        ) : null
                    }
                    <article dangerouslySetInnerHTML={{__html: data.markdownRemark.html}} />
                    {
                        data.markdownRemark.frontmatter.comments.length ? (
                            <article>
                                <h2>Comments</h2>
                                <ul>
                                    {data.markdownRemark.frontmatter.comments.map((comment: {author: string; content: string; date: string}) => (
                                        <li key={comment.date}>
                                            <h3>{comment.author}</h3>
                                            <p>{comment.content}</p>
                                            <p>{new Date(comment.date).toLocaleString()}</p>
                                        </li>
                                    ))}
                                </ul>
                            </article>
                        ) : null
                    }
                </Layout>
            </div>
    )
}

// export const Head = () => <Seo title="Super Cool Blog Posts" />
export const query = graphql`
  query ($id: String) {
    markdownRemark(id: {eq: $id}) {
      frontmatter {
        title
        comments {
          author
          content
          date
        }
      }
      html
    }
  }`
// export const query = graphql`
//   query ($filename: String) {
//     markdownRemark(
//         fields: {filename: {eq: $filename}}
//     ) {
//       frontmatter {
//         title
//       }
//       html
//     }
//   }
// `
export default BlogPost
