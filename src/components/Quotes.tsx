import * as React from 'react'
import {useEffect, useState} from 'react'
import {getRandomQuoteWithAuthor, highlightRandomText, quotesWithAuthor, shuffle} from '../data/quotes'

export default () => {
    const randomQuote = getRandomQuoteWithAuthor()
    const [quote, setQuote] = useState(randomQuote.quote)
    const [author, setAuthor] = useState(randomQuote.author)
    useEffect(() => {
        const quotes = shuffle(quotesWithAuthor)
        let index = 0
        const interval = setInterval(() => {
            const randomQuote = quotes[index++ % quotes.length]
            setQuote(randomQuote.quote)
            setAuthor(randomQuote.author)
        }, 10000)

        return () => clearInterval(interval)
    })
    return <div className={`blockquote-wrapper`}>
        <div className="blockquote">
            <h1 dangerouslySetInnerHTML={{__html: highlightRandomText(quote)}}/>
            <h4 dangerouslySetInnerHTML={{
                __html: '&mdash;' + highlightRandomText(author, 1) +
                    '<br/><em>Business quotes of the day</em>'
            }}/>
        </div>
    </div>
}
