import {create} from "zustand"
import {persist} from "zustand/middleware"

type NameType = 'dark' | 'light'

interface ThemeState {
    name: NameType
    setTheme: (name: NameType) => void;
}
export const useThemeStore = create<ThemeState>()(
    persist(set => ({
            name: 'dark',
            setTheme: (name: NameType) => set(() => ({ name: name }))
        }),
        { name: 'theme-storage'}
    )
)
