---
layout: post
status: publish
published: true
title: ! 'Howto: Install Apache Tomcat 6 on Ubuntu 11.10'
author:
  display_name: dcodernz
  login: dcodernz
  email: no-reply@thh.com
  url: ''
author_login: admin
author_email: no-reply@thh.com
wordpress_id: 178
wordpress_url: http://www.thheuer.com/?p=178
date: '2012-02-21 10:31:25 +1300'
date_gmt: '2012-02-20 22:31:25 +1300'
categories:
- Linux
- Howto
- Java
tags: [Linux,Howto,Java,Programming]
comments: []
---

<p>This post describes how to instal Apache Tomcat 6 on Ubuntu 11.10.</p>

<ol style="margin-top:40px">
<li>Download Tomcat 6 from http://tomcat.apache.org/download-60.cgi or a mirror listed there.</li>
<li>Extract it in the /usr/local directory:<br/>

```bash
cd /usr/local/
sudo tar xvfz ~/apache-tomcat-6.0.35.tar.gz # or wherever you downloaded tomcat to
sudo ln -s apache-tomcat-6.0.35 apache-tomcat-6 # or whatever your tomcat version is
```

</li>
</ol>
<p>This post is incomplete, but might get you going. More docs can be found @ <a href="http://www.eclipse.org/forums/index.php/t/272457/" target="_blank">http://www.eclipse.org/forums/index.php/t/272457/</a></p>
