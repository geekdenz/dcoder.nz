---
layout: post
status: publish
published: true
title: "Bitcoin - Crypto Currencies Facts and Thoughts"
author:
  display_name: dcodernz
  login: dcodernz
  email: info@dcoder.co.nz
  url: http://dcoder.nz
author_login: dcodernz
author_email: info@dcoder.co.nz
author_url: http://dcoder.nz
categories:
- Philosophy
- Computer Science
- Value
tags: [computer,science,philosophy,crypto,currency,value,bitcoin]
comments: []
---

[Crypto currencies](https://en.wikipedia.org/wiki/Cryptocurrency) are the talk of the century. Value and any other transactions
can occur and be validated on a network.

Bitcoin - Crypto Currencies & Blockchain Facts and Thoughts
===========================================================

As a controversial topic, I thought I would give my two cents or should I say: My two
[Satoshis](https://en.wikipedia.org/wiki/Bitcoin#Units) on the topic. My background is in Computer Science, [Distributed Systems](https://en.wikipedia.org/wiki/Distributed_computing), Mathematics such as
[Discrete Mathematics](https://en.wikipedia.org/wiki/Discrete_mathematics), yes, Cryptography as well as Information Systems like 
[Middleware](https://en.wikipedia.org/wiki/Middleware) and databases. I have read the [white paper on Bitcoin](https://bitcoin.org/bitcoin.pdf) and also the book
["Mastering Bitcoin" by Andreas M. Antonopoulos](https://www.bitcoinbook.info/), who also spent most of his life
learning about cryptography, networks such as [Distributed Systems](https://en.wikipedia.org/wiki/Distributed_computing), Mathematics
and yes, Crypto Currencies. I can say that I am an A student in Information Systems Security and
Discrete Mathematics and all of the above and have come from a script kiddy background to what I believe
is a [white hat hacker](https://en.wikipedia.org/wiki/White_hat_%28computer_security%29) now. I actually like to think of me as a gray hat hacker, because, common, who is only good all the time ;-). I do not claim to understand all the details of even
Bitcoin or other Crypto Currencies. It is a full-time job. However, I believe that I 
have a firm grasp on logic, mathematics, cryptography and what blockchain technology is
and how it works. I am not a financial expert, but also believe I have a grasp on the
topic.

Everyone, in a nutshell, knows what Bitcoin is. It is the first Crypto Currency. It is
best understood and most popular. It has almost 50% of the Crypto market share.
It is an open source project supported by
almost 500 contributors. Note that the Linux Kernel is the biggest, most supported software (not only Open Source!)
project that has ever existed. It has about 14,000 contributors. So, Bitcoin has developer support
of about 3.6% of the biggest software project mankind has and probably will see. That is huge!

It has parameters that are melded into the protocol and cannot
be changed, because they are melded into the first block.
Like the 21 million limit on coins. Its parameters are based on game theory
by the famous mathematician John Nash, who won the nobel prize for his efforts.
The mathematics and cryptography its security is based on is what underlies all modern
cryptography used by all banks, national security agencies and everything that
we believe in is secure. The hashing algorithms used are the most secure that mankind
has come up with so far plus the blockchain, the data structure that revolutionises
everything that involves transactions of any sort make the foundation, that hundreds
if not thousands of security experts confirmed to be secure. Furthermore, Bitcoin itself
has not been hacked for almost 9 years now. If there was an attack vector, it probably
would have been found already. Of course there is the 51% attack, but that is well
known and possible in any consensus protocol.

However, there is, currently more than ever, a weakness of Bitcoin. The amount of
transactions per seconds is far lower than that of databases or is acceptable
for real world use by everyone. Databases are simple
compared to blockchain technology, which is also, why databases are and always will
be hackable or at least compromisable by their creators or controllers.

Other crypto systems promise to solve the scalability problems. However, it seems
to have almost gone unnoticed, that Bitcoin has a solution for this as well called
the Lightning Network and Segwit. Now these are not just buzzwords, they are valid
technologies and promise to solve the scalability problems. [Lightning has recently
been tested and confirmed working on the blockchain](https://cointelegraph.com/news/bitfury-lightening-network-successfully-tested-with-french-bitcoin-company). A Crypto Currency will
never scale as easily as a database, but that is not the point of crypto.

A database can never be secured from its system admins and developers maintaining it.
Credit card details, bank accounts, all this type of information is stored by multiple
parties. Every single online store you used has the ability to store and retrieve
the information you entered. And if you haven't noticed, your credit card details
don't change, at least for a few years. Bitcoins on the other hand change with every
transaction that is ever made. In fact, the Bitcoins you own are not even in your
wallet. Your wallet just contains private keys to sign the transactions you make
and your received funds can only be spent with your private keys.

###Comments on the recent exit of one of the founders of Bitcoin.com:

Bitcoin.com is NOT Bitcoin! It is a company invested in Bitcoin.

A founder of a company, legitimately, also the founder of Bitcoin.com is after
returning a profit of whatever it is that is most valuable to them.
It is still hard to spend Bitcoin on anything one might want.
Bitcoin is nowhere near everywhere accepted. And there is currently this transactions
per second limit which combined with demand, drives the transaction costs up.
As a founder of Bitcoin.com (not Bitcoin!), he understands that something needs
to happen. And since he does not seem to know an algorithm to fix Bitcoin Core,
he bailed out. This could have had the following reasons:

 * He wants the best for Bitcoin, so wants to stop more people getting into Bitcoin
   so quickly potentially driving Bitcoin exchange to a halt and stopping the
   whole thing while it's being repaired.
 * He understands his power and thought: "I can dump my Bitcoin, the price will
   fall rapidly when the news gets out and then I can get some cash, get rich
   and buy back the same amount of Bitcoin, Bitcoin Cash or whatever crypto
   currency I fancy at the moment and basically double my money".
 * He is very uncertain about Bitcoin's future, because many are and saw
   an opportunity to get out rich.

After all, he is a young human being, I believe only 22 years old if I read
correctly and driven by many things.

###Hashgraph and other emerging technologies

Bitcoin or other blockchain technologies are still in their infancy and are already being
challenged by other technologies. Hashgraph is the most recent one I have heard
of. While Hashgraph is intriguing and I believe works from what I understand,
Hashgraph has a proprietary license which really only makes it viable for
B2B (Business to Business) such as banks. Something else I found as a weakness
was that the main website, hashgraph.com had a mixed content warning, at least
on my machine when I read it. A company that develops a security algorithm that has not proper
SSL (HTTPS) support on their website and promotes its algorithm as a
proprietary SECURITY! technology seems a bit dodgy to me.

However, this may change in the future or other
people, knowing the Hashgraph algorithm now, can come up with something new
and make that completely free and Open Source. Heck, the ideas of Hashgraph
could contribute to Bitcoin and other Crypto Currencies to fix the problems they
have such as scalability. I am 99% sure that there will be a solution to the
scaling problem of Bitcoin and that Bitcoin can be fixed in one form or another.
Maybe a fork will happen again. But remember, the Bitcoin one buys now will be
available on the fork to you, which essentially doubles your money, however
much value it then has. Note that for this to be the case you absolutely need
to control your own private keys! Bitcoin Cash is a fork of Bitcoin Core and
the people who had Bitcoin before the fork earned the same amount of Bitcoin Cash
that they had in Bitcoin Core. I know, this is confusing. So many currencies, all springing
out of nowhere with no real value attached to them other than people believing in
them and mathematicians telling us that they are proven to validate reliably?
Sounds like a scam, but I am certain it is not. Just out of interest, I have
invested some time in Bitcoin, reading the white paper back in 2010. I then
had doubts that this may work, at the time mainly because of the dependency on
a secure hash function and MD5 and other hash functions have been found to be
insecure. However, Bitcoin and other cryptos use [multiple hashes](https://en.bitcoin.it/wiki/How_bitcoin_works#Cryptography) that are 
believed to be secure now and are used in SSL and TLS certificates. If the
hash functions are the security problem, we have far bigger problems than
Bitcoin going bust. When I read the white paper the first time, as I said, I
did not believe this would work. Bitcoin had only been around for a year then
and I didn't dare to invest in something that I cannot be certain works.
I still cannot be 100% there won't be a fundamental flaw stopping the whole
system. However, time has proven that it does indeed work. Millions of people
believe in the blcokchain technology underlying Bitcoin not only because of
pure faith, but because it has proven itself over time and in mathematical
algorithms. It has had an extensive test of time and the problems with it are
engineering problems that imho will be solved.

### Cost

When I first heard about Bitcoin and blockchain I thought:
"OMG, think of all this electricity that is wasted!". I am an environmentalist
and work for an environmental organisation to do my part in saving the planet
from climate change and trying to at least postpone the certain doom of our
species, at least on this planet.

This is one of the major arguments of an [article I read just recently](https://www.stuff.co.nz/business/industries/99970394/investing-in-bitcoin-is-indefensible). The person who
wrote the article clearly does not understand the potential that blockchain
has unleashed like I did when I first heard about it.
Think of all the effort of mankind that goes into validating
truths and transactions! Not only human time, but precious resources such as
paper, water and electricity are consumed in the process.
I don't need to come to you with numbers, because, as we all know, people pull
statistics from everywhere to make a point. I believe in the logic that we can
attain by pure reasoning is much more valid than even mathematics that are
proven that we do not understand.

The point is that by pure reasoning we can understand that much more energy and
time will go into validating contracts and money transactions than any blockchain
can use in the same time it validates those transactions. I bought a house
recently and the amount of paper work and human time involved was outrages.
Imagine all this could happen with smart contracts and value transactions that
self-execute and take care of all this boring stuff while being more reliable,
more secure and letting people get on with life. Sure there still have to be
a seller and buyer and they have to agree on something, but the whole process
could be so much easier.

### Conclusions & thoughts

None of this demotes blockchain technologies or even Bitcoin. The things
that threaten blockchain and therefore all alt coins as well, is scalability.
Bitcoin Cash just has a bigger block size, which just postpones the problem for
example.

If the scalability problem is solved, which I strongly believe is possible,
the blockchain technologies that adapt it will be the winners. Maybe the solution
can be applied to all alt coins including Bitcoin. Maybe there just needs to
be a hard fork. However it will play out, I strongly believe that Bitcoin Core
is not flawed enough to fail or even devalue quickly like a bubble.
It is based on mathematics and strong computer security. It has trust of many 
people and has grown into an economy of its own. It has been working for 9
years, hasn't been hacked or crashed. A lot of work has gone into it. A lot of
human effort and time as well as machine time. Hardware has been created
specifically to mine blockchain coins and technologies. Smart people are invested
in Bitcoin related technologies. Bitcoin, blockchain
or in its most basic sense Distributed Consensus Algorithms are not going away
because of some news in the media. After all, media can be made up, but 
mathematics is ingrained in nature and cannot be put away as bull shit.

This is why I believe in Crypto Currencies or more precisely blockchain in the
long term and I believe Bitcoin Core or at least future forks are not stoppable
now and will at least retain or more likely increase massively in value.

If people spent every dollar they spend on lotto on a crypto currency, their
chances of winning, by my estimates, are at least 1 million times higher.

Now or in a few days when it's at its lowest, is a good time to invest btw.
Yet another media craze is happening right now, people who don't understand
Bitcoin will plunge either way of what they hear. ;-)

### Resources

You can find some more up-to-date information at

[Commodity.com's Bitcoin](https://commodity.com/cryptocurrency/bitcoin/) topic.
