---
layout: post
status: publish
published: true
author:
  display_name: dcodernz
  login: dcodernz
  email: info@dcoder.co.nz
  url: http://www.dcoder.co.nz
author_login: dcodernz
author_email: info@dcoder.co.nz
author_url: http://www.dcoder.co.nz
categories:
- Announcement
tags: [Announcement,Web,Development,HTML5,PHP]
comments: []
date: "2014-07-13"
title: "Announcing web development"
---

We're proudly announcing Web Development to be the main focus of this company.
The web is a fast growing platform that is accessible from anywhere. Mobile,
the desktop, your TV and any device that can display web pages like google.com.

To be a successful business going forward, you need a website if not a
sophisticated web applications to meet your customer needs. We specialise in web
development and can deliver fast, beautiful and useful web pages and
applications. Whether you need a simple, static web page, a simple blog with
semi-static pages that you want us to update for you or a fully content
managed website that you can update yourself with an easy to use, productive
back end and CMS, we can develop a solution for you that works best.

For as little as NZ$1500 we can develop a static web page for you that we can
then update for you for only $50 per blog post and $200 per new page design
to fit into this template.

A full-featured CMS Website development with custom development and/or a
simple or sophisticated shopping cart for your online shop can be developed for
as little as NZ$4000. Talk to us and you can find out what we can do within your
budget.

Let us help  you grow in this exciting area by deciding to make your app
or website with us. We look forward to your feedback and working with you.

Kind regards,
The Web Team
