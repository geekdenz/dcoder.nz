---
layout: post
status: publish
published: true
title: "Competitive Programming - My Journey"
author:
  display_name: dcodernz
  login: dcodernz
  email: info@dcoder.co.nz
  url: http://dcoder.nz
author_login: dcodernz
author_email: info@dcoder.co.nz
author_url: http://dcoder.nz
categories:
- CP
- Computer Science
tags: [computer,science,cp]
comments: []
---

Competitive Programming is about fun. And that is the main thing it should be about.

Competitive Programming - My Journey
====================================

Competitive Programming is first and foremost about having fun. Secondarily, it
is a hobby that helps you stay or become professionally relevant.

The reason I started with CP was that I stumbled upon something called
Google Code Jam about 7 years back. I thought it was interesting that one could
compete in what I perceived as only work until then. I thought I was a pretty
good programmer then. However, doing the competitiion and doing worse than about
50% of the participants, I realised: "OH! This is hard! You have to actually put
in quite a bit of effort to become good at this. So, I did. A year passt and not
having had much extra time to put into getting better and not knowing what to
even do to become better at it fast, I did another Google Code Jam.

Having done a few GCJ previous problems now and having some more experience as
a professional programmer/web developer, I did a lot better. In the top 45% or
so. I was pretty happy with myself but still wasn't really great. Then I lost
motivation, because I thought I cannot really get better at this and decided,
I was an OK programmer and could go on with just work.

Quite a few years later, and without further practice, I rediscovered this sport.
Around the same time, I discovered YouTube coding channels and got hooked. I followed
MPJ from FunFunFunction and was learning a lot about functional programming, JavaScript
and many other programming languages such as Python, Java, PHP, TypeScript and more 
obscure languages.
MPJ then gave me an epiphany: "As programmers, we should not perceive programming as
work, but as a calling and therefore should do it also sometimes just for its own
sake. We should program just for fun!"

OK, having heard that, I thought I'd just try that: Don't just program for work
but for recreation. I learnt a lot about design patterns, JavaScript and some
tools to make it more fun. Then I discovered, also thanks to MPJ, Quokka.
Having used it ever since for trying something quickly in JavaScript or
TypeScript, I programmed a tool called cutr (cut by regular expression)
and a library called treehof (Tree Higher Order Functions) for iterating over
tree datastructures.

Little did I know, I was only getting started and really just scratched the
surface. Programming is a rabit hole that cannot be exhaustively searched as
there are an infinite amount of algorithms, and this is part of the beauty of
it. It is always, again and again humbling and one can always reach one's
limits easily, but then also expand beyond them and achieve further enlightenment.

So, I went and committed to becoming a competitive programmer. Not with the goal
to become the best, as that is probably too late for me now, but to become the
best programmer I could be from that point onwards in my life.

The more I learn about CP, the happier I become, also with just doing my day-job
as a web developer.

So, what was the next step? I knew C, some C++, Java and a little Python. Java
seemed like the choice for me, because it is fast and has a vast library of
algorithms and datastructures. So, I spent a long time learning most of Java.
I would not claim my knowledge is complete in any language as programming languages,
just like natural ones, keep evolving.

However, I have quite a good grasp of most of the Java language now. But then, ...
it came to me, I found the website called HackerRank. Wow! So much to learn!
I spent several hours doing problem after problem in Java. But then, digging
deeper and deeper into the CP rabit hole, it became clear to me, that Java
was just one language. To become a better or even great competitive programmer,
I would need to learn also C++. I then spend a few weeks doing just that. I learnt
C++ by doing the complete course on HackerRank and made it to equal rank with
the number one on HackerRank in C++ and got a pretty good understanding of the
STL as well. The STL, the holy grail for a competitive programmer. So many problems
just become trivial when one just knows the STL well or at least how to look things
up. IMHO it has the most comprehensive and efficient implementations of the most
important CP algorithms and datastructures (and therefore for most problems).
I do not claim I know the whole STL, but I am getting pretty good at looking it
up and understanding the documentation quickly, which is super helpful when
solving a CP problem.

Along the way, of course, I had to pick up the fact that most CP problems also
require a good understanding of the broad field of mathematics. Luckily, I have
always had a knick for solving mathematical problems. I would not say I am a
genius but have the necessary obsession and drive to learn new concepts quickly.

Now it became clear to me, the more I learnt, the more there was to learn. But the
good thing is, the more you learn, the quicker it gets to learn new things and it
is very rewarding and empowering to learn a new concept.

Since then I have been participating in a quite a few coding competitions. The best I
did so far was in the top 20% in a competition that started at 4:30am in the morning
in my timezone. There were 3 people representing New Zealand and I was first out of
those 3 finishing in about half the time of the next one. The last competition I
participated in I placed in the top 23% on LeetCode.

I have been reading many books, although probably not as dedicated and fast as I would
have hoped. But C++ is now my favourite language for CP, followed by Python for problems
that require large number support or combinatorics.

There is more to discover and I would like to cover topics within CP such as specific
algorithms and datastructures and my take on them here.

If you are into CP and would like to read more about this, I would be happy to get your
feedback on my Twitter or email. Please don't hesitate to get in touch.

All the best.