---
layout: post
status: publish
published: true
title: GeoExt and OpenLayers - Zoom Mode
author:
  display_name: dcodernz
  login: dcodernz
  email: no-reply@thh.com
  url: http://www.ihostnz.com
author_login: dcodernz
author_email: no-reply@thh.com
author_url: http://www.ihostnz.com
wordpress_id: 18
wordpress_url: http://www.ihostnz.com/blog/?p=18
date: '2010-08-03 16:14:20 +1200'
date_gmt: '2010-08-03 04:14:20 +1200'
categories:
- JavaScript
- Programming
tags: [JavaScript,Programming,GIS]
comments: []
---

<p>Just playing around with GeoExt and OpenLayers, I figured out how to easily change the control to a zoom rectangle with minimal changes:</p>

<p>First create the normal and the custom control with:</p>
```javascript

// javascript
var inZoom = false;
var navigation = new OpenLayers.Control.Navigation();
var ZoomBoxNav = OpenLayers.Class(OpenLayers.Control.Navigation, {
 zoomBoxKeyMask: null // this ensures that a box is always drawn
});
var zoomBox = new ZoomBoxNav();
```

<p>Then all you need is a button or better Action object, which adds the appropriate control to the map:</p>
```javascript

// javascript
var toggleRectZoom = function() {
  if (inZoom) {
    map.addControl(navigation);
    map.removeControl(zoomBox);
    inZoom = false;
  } else {
    map.addControl(zoomBox);
    map.removeControl(navigation);
    inZoom = true;
  }
}

zoomButton = new GeoExt.Action({
  text: 'Zoom Mode',
  handler: function() {
    if (inZoom) {
      zoomButton.setText('Zoom Mode');
    } else {
      zoomButton.setText('Pan Mode');
    }
    toggleRectZoom();
  }
});

toolBar = new Ext.Toolbar({
  items: [ zoomButton ]
});
mapPanel = new GeoExt.MapPanel({
  border: true,
  region: "center",
  // we do not want all overlays, to try the OverlayLayerContainer
  map: map,
  center: [172.1569825, -42.6109735],
  zoom: 6,
  layers: myLayers,
  tbar: toolBar
});
```

