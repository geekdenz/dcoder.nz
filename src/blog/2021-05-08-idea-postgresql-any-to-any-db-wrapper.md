---
layout: post
status: publish
published: true
title: "PostgreSQL/MySQL - Any-to-Any Database Wrapper"
author:
  display_name: dcodernz
  login: dcodernz
  email: info@dcoder.co.nz
  url: http://dcoder.nz
author_login: dcodernz
author_email: info@dcoder.co.nz
author_url: http://dcoder.nz
categories:
- Database
- Computer Science
- MySQL
- PostgreSQL
- Middleware
tags: [computer,science,database,mysql,postgresql,middleware]
comments: []
---

PostgreSQL/MySQL - Any-to-Any Database Wrapper.

Similar to Wine and the gaming ideas on Linux.


PostgreSQL/MySQL - Any-to-Any Database Wrapper
==============================================

Similar to the gaming concepts of, I believe, Vulkan, Wine and Cedega, we could abstract out databases not on an ORM level,
but on the DBMS protocol level.

Often, organisations support one database management system (DBMS), a relational DBMS such as
PostgreSQL, MySQL or other proprietary DBMS, which I am reluctant to mention.

What if we could write a middleware that would act as a server of any DBMS protocol? That
middleware could then replace the DBMS in question with a thin O(1) layer and just use the one
the organisation wants. O(1) because prepared statements would be cached and in a hashmap that
could provide O(1) added latency that could be sub-milliseconds and would not contribute to
the bulk of the cost of the query.

This middleware could be written in any language as it would add only O(1) overhead to any DB queries
posed by the client. This seems to work quite well for even games that have sub-ms queries and still
run with near-native performance.

It would depend on the languages and libraries that they have. Maybe MariaDB and PostgreSQL have some
code that could be re-used.

The prepared statements of queries could be cached in an efficient DB table that itself could work
on any DBMS or just PostgreSQL for example.

It might be easiest to write it as a PostgreSQL extension that runs on a port and provides the MySQL
API.

I think for now I would only use Regular Expressions, but later something like ANTLR might be the way
to go.

OK, enough talk ... I think I might actually start trying this out in a proof-of-concept for a small
subset of queries.

