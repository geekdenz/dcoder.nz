// Get a reference to the storage service, which is used to create references in your storage bucket
//var storage = firebase.storage();

// Create a storage reference from our storage service
//var storageRef = storage.ref();

// Get a reference to the database service
var database = firebase.database();
function writeMessage(key, message) {
  database.ref('kvp/' + key).set({
	  message: message
  });
}
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}
function getRandomIntArray(min, max, n) {
	var a = [];
	var current = min;
	var width = (max - min) / n;
	var currentInterval = min;
	for (var i = 0; i < n; i++) {
		current += Math.random() * width;
		a.push(current);
		currentInterval = i * width + min;
		current = currentInterval + width;
	}
	return a;
}
function startAnimations(message) {
	//var balloon = $(svgString);
	var el = $('.main-text');
	el.html(decodeURIComponent(message));
	//var balloon = document.importNode(svgDoc.documentElement,true);
	//$(window).append(balloon);
	TweenMax.from(el, 12, {
		opacity: 0
	});
	//$('#balloon').show();
	var balloon = $('.balloon');
	//balloon.hide();
	var $body = $('body');
	var balloons = [];
	var amount = 40;
	var wwidth = $(window).innerWidth();
	var distribution = getRandomIntArray(0, wwidth, amount);
	var deg = 360 / amount;
	var rot = 0;
	var colorRotations = [];
	for (var i = 0; i < amount; i++) {
		colorRotations.push(rot);
		rot += deg;
	}
	for (var i = 0; i < amount; i++) {
		var newBalloon = balloon.clone();
		newBalloon.attr('id', 'svg' + i);
		newBalloon.removeClass('hidden');
		newBalloon.css('filter', 'hue-rotate(' + colorRotations[Math.floor(Math.random() * amount)] + 'deg)');
		newBalloon.addClass('new');
		newBalloon.addClass('color-' + i);
		var offset = distribution[i];
		newBalloon.css('left', offset);
		$body.append(newBalloon);
		balloons.push(newBalloon);
	}
	$('.balloon.new').each(function(i, e) {
		var bezier = [
			{ x: 100, y: 0 },
			{ x: 0, y: 50 },
			{ x: 100, y: 100 },
			{ x: 0, y: 150 },
			{ x: 100, y: 200 },
			{ x: 0, y: 250 },
			{ x: 100, y: 300 },
			{ x: 0, y: 350 },
			{ x: 100, y: 400 },
			{ x: 0, y: 450 },
			{ x: 100, y: 500 },
			{ x: 0, y: 550 },
			{ x: 100, y: 600 },
			{ x: 0, y: 650 }
		];
		$(e).css('top', Math.random() * 100);
		for (var i = 0; i < bezier.length; i++) {
			bezier[i].x += (i - 1)*50 + (Math.random() * 100);
			bezier[i].y += (i - 1)*50 + (Math.random() * 50);
		}
		TweenMax.from(e, 4, {
			top: 1000,
			ease: Circ.easeOut,
			bezier: {
				type: 'thru',
				values: bezier,
				curviness: 0
			}
		});
	});
}
if (document.location.search) {
	var parts = document.location.search.split('=');
	var key = parts[0];
	var value = "getting value";
	if (parts.length === 1) {
		database.ref('kvp/' + key).once('value').then(function(snapshot) {
			value = snapshot.val().message;
			/*
			$.get('balloon.svg', 'xml').then(function(svgDoc) {
				startAnimations(value, svgDoc);
			}); 
			*/
			//var svgUrl = 'balloon.svg';
			startAnimations(value);
		});
	}	else if (parts.length === 2) {
		value = parts[1];
		writeMessage(key, value);
		startAnimations(value);
	}
}
